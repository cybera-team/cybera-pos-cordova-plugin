/********* CyberaPOS.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <StarIO/SMPort.h>
#import <StarIO_Extension/StarIoExtManager.h>
#import "AppDelegate+pos.h"
#import "ModelCapability.h"
#import "Communication.h"

@interface CyberaPOS : CDVPlugin<StarIoExtManagerDelegate> {
  // Member variables go here.
}
@property (nonatomic) StarIoExtManager *starIoExtManager;

- (void)searchPOS:(CDVInvokedUrlCommand*)command;
- (void)getPrinter:(CDVInvokedUrlCommand*)command;
- (void)onBarcodeDataReceive:(CDVInvokedUrlCommand*)command;
- (void)registerPrinter:(CDVInvokedUrlCommand*)command;
- (void)openCashDrawer:(CDVInvokedUrlCommand *)command;
- (void)print:(CDVInvokedUrlCommand *) command;
@end

@implementation CyberaPOS

static NSString *dataCallbackId = nil;

- (void)pluginInitialize
{
    if (_starIoExtManager == nil) {
        _starIoExtManager = [[StarIoExtManager alloc] initWithType:StarIoExtManagerTypeWithBarcodeReader
                                                          portName:[AppDelegate getPortName]
                                                      portSettings:@""
                                                   ioTimeoutMillis:10000];

        _starIoExtManager.delegate = self;
    }

    if (_starIoExtManager.port != nil) {
        [_starIoExtManager disconnect];
    }
    [_starIoExtManager connect];
}

- (void)searchPOS:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSArray* ports = [SMPort searchPrinter];
        NSMutableArray* result = [NSMutableArray arrayWithCapacity:[ports count]];
        [ports enumerateObjectsUsingBlock:^(PortInfo*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary* row = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    obj.macAddress,@"macAddress",
                                    obj.portName,@"name",
                                    obj.modelName,@"modelName"
                                    , nil];
            [result addObject:row];
        }];

        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                         [result copy],@"ports",
                         nil]];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
- (void)getPrinter:(CDVInvokedUrlCommand*)command{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* portName =[AppDelegate getPortName];
        if(portName == nil || [portName isEqualToString:@""]){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Not connected to printer"];
        }else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:[[NSDictionary alloc] initWithObjectsAndKeys:portName,@"name", nil]];
        }

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    }];
}
- (void)onBarcodeDataReceive:(CDVInvokedUrlCommand*)command{
    dataCallbackId = command.callbackId;
}
- (void)registerPrinter:(CDVInvokedUrlCommand*)command{
    NSString* portName = [command.arguments objectAtIndex:0];
    NSString* macAddress = [command.arguments objectAtIndex:1];
    NSString* modelName  = [command.arguments objectAtIndex:2];

    ModelIndex modelIndex = [ModelCapability modelIndexAtModelName:modelName];

    NSString           *portSettings = [ModelCapability portSettingsAtModelIndex:modelIndex];
    StarIoExtEmulation  emulation    = [ModelCapability emulationAtModelIndex   :modelIndex];



    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        [AppDelegate setPortName    :portName];
        [AppDelegate setModelName   :modelName];
        [AppDelegate setMacAddress  :macAddress];
        [AppDelegate setPortSettings:portSettings];
        [AppDelegate setEmulation   :emulation];


        if (_starIoExtManager != nil) {
            if (_starIoExtManager.port != nil) {
                [_starIoExtManager disconnect];
            }
            _starIoExtManager = nil;
        }
        if (_starIoExtManager == nil) {
            _starIoExtManager = [[StarIoExtManager alloc] initWithType:StarIoExtManagerTypeWithBarcodeReader
                                                              portName:[AppDelegate getPortName]
                                                          portSettings:@""
                                                       ioTimeoutMillis:10000];

            _starIoExtManager.delegate = self;
        }

        if (_starIoExtManager.port != nil) {
            [_starIoExtManager disconnect];
        }
        [_starIoExtManager connect];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:
                        [[NSDictionary alloc] initWithObjectsAndKeys:
                         portName,@"name",
                         macAddress,@"macAddress",
                         modelName,@"modelName",
                         nil]];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}
-(void)openCashDrawer:(CDVInvokedUrlCommand *)command{
    [self.commandDelegate runInBackground:^{
        SMPort *port = nil;

        port = [_starIoExtManager port];

        unsigned char openCashDrawerCommand = 0x07;


        NSData *commandData = [NSData dataWithBytes:&openCashDrawerCommand length:1];

        if (_starIoExtManager != nil) {
            [_starIoExtManager.lock lock];
        }

        BOOL printResult = [Communication sendCommands:commandData port:port];

        if (_starIoExtManager != nil) {
            [_starIoExtManager.lock unlock];
        }
        CDVPluginResult	*result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:printResult];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }];
}
- (void)print:(CDVInvokedUrlCommand *) command{
    [self.commandDelegate runInBackground:^{
        NSMutableData *commands = [NSMutableData data];
        NSString *content = @"";
        SMPort *port = nil;
        NSInteger width = [AppDelegate getSelectedPaperSize];
        if (command.arguments.count > 0) {
            content = [command.arguments objectAtIndex:0];
        }

        port = [_starIoExtManager port];
        UIFont *font = [UIFont fontWithName:@"Menlo" size:11 * 2];
        NSDictionary *attributeDic = @{NSFontAttributeName:font};

        CGSize size = [content boundingRectWithSize:CGSizeMake(width, 10000)
                                           options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine
                                        attributes:attributeDic
                                           context:nil].size;

        if ([UIScreen.mainScreen respondsToSelector:@selector(scale)]) {
            if (UIScreen.mainScreen.scale == 2.0) {
                UIGraphicsBeginImageContextWithOptions(size, NO, 1.0);
            } else {
                UIGraphicsBeginImageContext(size);
            }
        } else {
            UIGraphicsBeginImageContext(size);
        }

        CGContextRef context = UIGraphicsGetCurrentContext();

        [[UIColor whiteColor] set];

        CGRect rect = CGRectMake(0, 0, size.width + 1, size.height + 1);

        CGContextFillRect(context, rect);

        NSDictionary *attributes = @ {
        NSForegroundColorAttributeName:[UIColor blackColor],
        NSFontAttributeName:font
        };

        [content drawInRect:rect withAttributes:attributes];

        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();

        UIGraphicsEndImageContext();


        ISCBBuilder *builder = [StarIoExt createCommandBuilder:[AppDelegate getEmulation]];

        [builder beginDocument];


        [builder appendBitmap:image diffusion:NO];

        [builder appendCutPaper:SCBCutPaperActionPartialCutWithFeed];

        [builder endDocument];

        [builder.commands copy];

        commands = [builder.commands copy];

        if (_starIoExtManager != nil) {
            [_starIoExtManager.lock lock];
        }

        BOOL printResult = [Communication sendCommands:commands port:port];

        if (_starIoExtManager != nil) {
            [_starIoExtManager.lock unlock];
        }

        CDVPluginResult	*result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:printResult];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }];
}




//Printer events
-(void)didPrinterCoverOpen {
    NSLog(@"printerCoverOpen");
    //[self sendData:@"printerCoverOpen" data:nil];
}

-(void)didPrinterCoverClose {
    NSLog(@"printerCoverClose");
    //[self sendData:@"printerCoverClose" data:nil];
}

-(void)didPrinterImpossible {
    NSLog(@"printerImpossible");
    //[self sendData:@"printerImpossible" data:nil];
}

-(void)didPrinterOnline {
    NSLog(@"printerOnline");
    //[self sendData:@"printerOnline" data:nil];
}

-(void)didPrinterOffline {
    NSLog(@"printerOffline");
    //[self sendData:@"printerOffline" data:nil];
}

-(void)didPrinterPaperEmpty {
    NSLog(@"printerPaperEmpty");
    //[self sendData:@"printerPaperEmpty" data:nil];
}

-(void)didPrinterPaperNearEmpty {
    NSLog(@"printerPaperNearEmpty");
    //[self sendData:@"printerPaperNearEmpty" data:nil];
}

-(void)didPrinterPaperReady {
    NSLog(@"printerPaperReady");
    //[self sendData:@"printerPaperReady" data:nil];
}


//Barcode reader events
-(void)didBarcodeReaderConnect {
    NSLog(@"barcodeReaderConnect");
    //[self sendData:@"barcodeReaderConnect" data:nil];
}

-(void)didBarcodeDataReceive:(NSData *)data {
    NSLog(@"barcodeDataReceive");
    [self.commandDelegate runInBackground:^{
        NSMutableString *text = [NSMutableString stringWithString:@""];

        const uint8_t *p = [data bytes];

        for (int i = 0; i < data.length; i++) {
            uint8_t ch = *(p + i);

            if(ch >= 0x20 && ch <= 0x7f) {
                [text appendFormat:@"%c", (char) ch];
            }
            else if (ch == 0x0d) { //carriage return
                //            text = [NSMutableString stringWithString:@""];
            }
        }
        NSLog(@"barcode data recieved = %@",text);
        CDVPluginResult	*result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:text];
        [result setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:result callbackId:dataCallbackId];
    }];

}

-(void)didBarcodeReaderImpossible {
    NSLog(@"barcodeReaderImpossible");
    //[self sendData:@"barcodeReaderImpossible" data:nil];
}

//Cash drawer events
-(void)didCashDrawerOpen {
    NSLog(@"cashDrawerOpen");
    //[self sendData:@"cashDrawerOpen" data:nil];
}
-(void)didCashDrawerClose {
    NSLog(@"cashDrawerClose");
    //[self sendData:@"cashDrawerClose" data:nil];
}




//AppEvents


- (void)onAppTerminate
{
    NSLog(@"%@ onAppTerminate!", [self class]);
    if (_starIoExtManager != nil && _starIoExtManager.port != nil) {
        [_starIoExtManager disconnect];
    }
}

- (void)onMemoryWarning
{
    NSLog(@"%@ onMemoryWarning!", [self class]);
}

- (void)onReset
{
    NSLog(@"%@ onReset!", [self class]);
}

- (void)dispose
{
    NSLog(@"%@ dispose!", [self class]);
    if (_starIoExtManager != nil && _starIoExtManager.port != nil) {
        [_starIoExtManager disconnect];
    }
}

@end
