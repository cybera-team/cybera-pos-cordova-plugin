//
//  AppDelegate.m
//  ObjectiveC SDK
//
//  Created by Yuji on 2015/**/**.
//  Copyright © 2015年 Star Micronics. All rights reserved.
//

#import "AppDelegate+pos.h"
#import <objc/runtime.h>

@implementation AppDelegate(pos)

+ (void)load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = @selector(init);
        SEL swizzledSelector = @selector(posPluginSwizzledInit);
        
        Method original = class_getInstanceMethod(class, originalSelector);
        Method swizzled = class_getInstanceMethod(class, swizzledSelector);
        
        BOOL didAddMethod =
        class_addMethod(class,
                        originalSelector,
                        method_getImplementation(swizzled),
                        method_getTypeEncoding(swizzled));
        
        if (didAddMethod) {
            class_replaceMethod(class,
                                swizzledSelector,
                                method_getImplementation(original),
                                method_getTypeEncoding(original));
        } else {
            method_exchangeImplementations(original, swizzled);
        }
    });
}

- (AppDelegate *)posPluginSwizzledInit
{
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(posPluginOnApplicationDidBecomeActive:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    // This actually calls the original init method over in AppDelegate. Equivilent to calling super
    // on an overrided method, this is not recursive, although it appears that way. neat huh?
    return [self posPluginSwizzledInit];
}

- (void)posPluginOnApplicationDidBecomeActive:(UIApplication *)application{
    [NSThread sleepForTimeInterval:1.0];     // 1000mS!!!
    
    [self loadParam];
    [AppDelegate setSelectedIndex:0];
    [AppDelegate setSelectedLanguage:LanguageIndexEnglish];
    [AppDelegate setSelectedPaperSize: PaperSizeIndexTwoInch];
    
    
}

- (void)loadParam{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@""                           forKey:@"portName"]];
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@""                           forKey:@"portSettings"]];
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@""                           forKey:@"modelName"]];
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@""                           forKey:@"macAddress"]];
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@(StarIoExtEmulationStarPRNT) forKey:@"emulation"]];
    [userDefaults registerDefaults:[NSDictionary dictionaryWithObject:@0x00000007                   forKey:@"allReceiptsSettings"]];
    
    [AppDelegate setPortName:[userDefaults stringForKey :@"portName"]];
    [AppDelegate setPortSettings        : [userDefaults stringForKey :@"portSettings"]];
    [AppDelegate setModelName           : [userDefaults stringForKey :@"modelName"]];
    [AppDelegate setMacAddress          : [userDefaults stringForKey :@"macAddress"]];
    [AppDelegate setEmulation           : [userDefaults integerForKey:@"emulation"]];
    [AppDelegate setAllReceiptsSettings : [userDefaults integerForKey:@"allReceiptsSettings"]];

}

+ (NSString *)getPortName {
    return [[NSUserDefaults standardUserDefaults] stringForKey :@"portName"];
}

+ (void)setPortName:(NSString *)portName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:portName forKey:@"portName"];
    
    [userDefaults synchronize];
}

+ (NSString*)getPortSettings {
    return [[NSUserDefaults standardUserDefaults] stringForKey :@"portSettings"];
}

+ (void)setPortSettings:(NSString *)portSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject :portSettings forKey:@"portSettings"];
    
    [userDefaults synchronize];
}

+ (NSString *)getModelName {
    return [[NSUserDefaults standardUserDefaults] stringForKey :@"modelName"];}

+ (void)setModelName:(NSString *)modelName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:modelName forKey:@"modelName"];
    
    [userDefaults synchronize];
}

+ (NSString *)getMacAddress {
    return [[NSUserDefaults standardUserDefaults] stringForKey :@"macAddress"];}

+ (void)setMacAddress:(NSString *)macAddress {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:macAddress forKey:@"macAddress"];
    
    [userDefaults synchronize];
}

+ (StarIoExtEmulation)getEmulation {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"emulation"];
}

+ (void)setEmulation:(StarIoExtEmulation)emulation {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:emulation forKey:@"emulation"];
    
    [userDefaults synchronize];
}

+ (NSInteger)getAllReceiptsSettings {
    return [[NSUserDefaults standardUserDefaults] integerForKey :@"allReceiptsSetting"];
}

+ (void)setAllReceiptsSettings:(NSInteger)allReceiptsSettings {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:allReceiptsSettings forKey:@"allReceiptsSettings"];
    
    [userDefaults synchronize];
}

+ (NSInteger)getSelectedIndex {
    return [[NSUserDefaults standardUserDefaults] integerForKey :@"selectedIndex"];
}

+ (void)setSelectedIndex:(NSInteger)index {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:index forKey:@"selectedIndex"];
    
    [userDefaults synchronize];
}

+ (LanguageIndex)getSelectedLanguage {
    return [[NSUserDefaults standardUserDefaults] integerForKey :@"selectedLanguage"];
}

+ (void)setSelectedLanguage:(LanguageIndex)index {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:index forKey:@"selectedLanguage"];
    
    [userDefaults synchronize];
}

+ (PaperSizeIndex)getSelectedPaperSize {
    return [[NSUserDefaults standardUserDefaults] integerForKey :@"selectedPaperSize"];
}

+ (void)setSelectedPaperSize:(PaperSizeIndex)index {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:index forKey:@"selectedPaperSize"];
    
    [userDefaults synchronize];}
@end
