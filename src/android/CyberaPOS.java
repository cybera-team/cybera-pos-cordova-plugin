package th.in.elite.cybera.pos.CyberaPOS;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.starmicronics.stario.PortInfo;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.starioextension.commandbuilder.ISCBBuilder;
import com.starmicronics.starioextension.commandbuilder.SCBFactory;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManager;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManagerListener;


import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;


/**
 * This class echoes a string called from JavaScript.
 */
public class CyberaPOS extends CordovaPlugin {
  private StarIoExtManager PosManager;
  private CallbackContext didAccessoryConnectFailureCallback;
  private CallbackContext didAccessoryConnectSuccessCallback;
  private CallbackContext didAccessoryDisconnectCallback;
  private CallbackContext didBarcodeDataReceiveCallback;
  private CallbackContext didBarcodeReaderConnectCallback;
  private CallbackContext didBarcodeReaderDisconnectCallback;
  private CallbackContext didBarcodeReaderImpossibleCallback;
  private CallbackContext didCashDrawerCloseCallback;
  private CallbackContext didCashDrawerOpenCallback;
  private CallbackContext didPrinterCoverCloseCallback;
  private CallbackContext didPrinterCoverOpenCallback;
  private CallbackContext didPrinterImpossibleCallback;
  private CallbackContext didPrinterOfflineCallback;
  private CallbackContext didPrinterOnlineCallback;
  private CallbackContext didPrinterPaperEmptyCallback;
  private CallbackContext didPrinterPaperNearEmptyCallback;
  private CallbackContext didPrinterPaperReadyCallback;
  private final StarIoExtManagerListener mStarIoExtManagerListener = new StarIoExtManagerListener(){
    @Override
    public void didAccessoryConnectFailure() {
      super.didAccessoryConnectFailure();
      if(didAccessoryConnectFailureCallback!=null) {
        didAccessoryConnectFailureCallback.success();
      }
    }

    @Override
    public void didAccessoryConnectSuccess() {
      super.didAccessoryConnectSuccess();
      if(didAccessoryConnectSuccessCallback!=null) {
        didAccessoryConnectSuccessCallback.success();
      }
    }

    @Override
    public void didAccessoryDisconnect() {
      super.didAccessoryDisconnect();
      if(didAccessoryDisconnectCallback!=null)
        didAccessoryDisconnectCallback.success();
    }

    @Override
    public void didBarcodeDataReceive(byte[] bytes) {
      super.didBarcodeDataReceive(bytes);
      if(didBarcodeDataReceiveCallback!=null){
        String[] readString = new String(bytes).split("\n");
        PluginResult result = new PluginResult(PluginResult.Status.OK,readString[0]);
        result.setKeepCallback(true);
        didBarcodeDataReceiveCallback.sendPluginResult(result);


      }

    }

    @Override
    public void didBarcodeReaderConnect() {
      super.didBarcodeReaderConnect();
      if(didBarcodeReaderConnectCallback!=null)
        didBarcodeReaderConnectCallback.success();
    }

    @Override
    public void didBarcodeReaderDisconnect() {
      super.didBarcodeReaderDisconnect();
      if(didBarcodeReaderDisconnectCallback!=null)
        didBarcodeReaderDisconnectCallback.success();
    }

    @Override
    public void didBarcodeReaderImpossible() {
      super.didBarcodeReaderImpossible();
      if(didBarcodeReaderImpossibleCallback!=null)
        didBarcodeReaderImpossibleCallback.success();
    }

    @Override
    public void didCashDrawerClose() {
      super.didCashDrawerClose();
      if(didCashDrawerCloseCallback!=null)
        didCashDrawerCloseCallback.success();
    }

    @Override
    public void didCashDrawerOpen() {
      super.didCashDrawerOpen();
      if(didCashDrawerOpenCallback!=null)
        didCashDrawerOpenCallback.success();
    }

    @Override
    public void didPrinterCoverClose() {
      super.didPrinterCoverClose();
      if(didPrinterCoverCloseCallback!=null)
        didPrinterCoverCloseCallback.success();
    }

    @Override
    public void didPrinterCoverOpen() {
      super.didPrinterCoverOpen();
      if(didPrinterCoverOpenCallback!=null)
        didPrinterCoverOpenCallback.success();
    }

    @Override
    public void didPrinterImpossible() {
      super.didPrinterImpossible();
      if(didPrinterImpossibleCallback!=null)
        didPrinterImpossibleCallback.success();
    }

    @Override
    public void didPrinterOffline() {
      super.didPrinterOffline();
      if(didPrinterOfflineCallback!=null)
        didPrinterOfflineCallback.success();
    }

    @Override
    public void didPrinterOnline() {
      super.didPrinterOnline();
      if(didPrinterOnlineCallback!=null)
        didPrinterOnlineCallback.success();
    }

    @Override
    public void didPrinterPaperEmpty() {
      super.didPrinterPaperEmpty();
      if(didPrinterPaperEmptyCallback!=null)
        didPrinterPaperEmptyCallback.success();
    }

    @Override
    public void didPrinterPaperNearEmpty() {
      super.didPrinterPaperNearEmpty();
      if(didPrinterPaperNearEmptyCallback!=null)
        didPrinterPaperNearEmptyCallback.success();
    }

    @Override
    public void didPrinterPaperReady() {
      super.didPrinterPaperReady();
      if(didPrinterPaperReadyCallback!=null)
        didPrinterPaperReadyCallback.success();
    }


  };
  @Override
  public boolean execute(String action, JSONArray args, CallbackContext cbc) throws JSONException {
    final CallbackContext callbackContext = cbc;
    if (action.equals("coolMethod")) {
      String message = args.getString(0);

      this.coolMethod(message, callbackContext);
      return true;
    }
    else if(action.equals("searchPOS")){
      cordova.getThreadPool().execute(new Runnable() {
        public void run() {
          searchPos(callbackContext);
        }
      });

      return true;
    }
    else if(action.equals("registerPrinter")){
      final String name = args.getString(0);
      final String macAddress = args.getString(1);
      this.registerPrinter(name, macAddress, callbackContext);

      return true;
    }

    /***********************************************************/
    //add event listener
    else if(action.equals("onBarcodeDataReceive")){
      didBarcodeDataReceiveCallback = callbackContext;
      return true;
    }


    /**********************************************************/
    else {
      if(PosManager.getPort() != null){
        if(action.equals("getPrinter")){


          cordova.getThreadPool().execute(new Runnable() {
            public void run() {
              getPrinter(callbackContext);

            }
          });
          return true;
        }
        else if(action.equals("openCashDrawer")){

          cordova.getThreadPool().execute(new Runnable() {
            public void run() {
              openCashDrawer(callbackContext);

            }
          });
          return true;
        }else if(action.equals("print")){
          final String html = args.getString(0);

          cordova.getThreadPool().execute(new Runnable() {
            public void run() {
              print(html,callbackContext);

            }
          });
          return true;
        }
      }else{
        callbackContext.error("Not connected to printer");
      }
    }
    return false;
  }

  private static Bitmap createBitmapFromText(String printText, int textSize, int printWidth, Typeface typeface) {
    Paint paint = new Paint();
    Bitmap bitmap;
    Canvas canvas;

    paint.setTextSize(textSize);
    paint.setTypeface(typeface);

    paint.getTextBounds(printText, 0, printText.length(), new Rect());

    TextPaint textPaint = new TextPaint(paint);
    android.text.StaticLayout staticLayout = new StaticLayout(printText, textPaint, printWidth, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

    // Create bitmap
    bitmap = Bitmap.createBitmap(staticLayout.getWidth(), staticLayout.getHeight(), Bitmap.Config.ARGB_8888);

    // Create canvas
    canvas = new Canvas(bitmap);
    canvas.drawColor(Color.WHITE);
    canvas.translate(0, 0);
    staticLayout.draw(canvas);

    return bitmap;
  }
  private void print(String html, CallbackContext callbackContext) {
    if (html != null && html.length() > 0) {
      CommandDataList commands = new CommandDataList();

      String textToPrint = html;

      int      textSize   = 22;
      Typeface typeface   = Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL);
      Bitmap   bitmap     = createBitmapFromText(textToPrint, textSize, 384, typeface);



      ISCBBuilder builder = SCBFactory.createBuilder(SCBFactory.Emulation.Star);
      builder.appendBitmap(bitmap, false, 384);

      List<byte[]> listBuf = builder.getBuffer();

      for(byte[] buf:listBuf) {
        commands.add(buf);
      }

      commands.add(0x1b, 0x64, 0x03);                    // Cut Paper


      Communication.Result result = Communication.sendCommands(commands.getByteArray(), PosManager.getPort(), this.cordova.getActivity());
      String msg = "";
      switch (result) {
        case Success:
          msg = "Success!";
          break;
        case ErrorOpenPort:
          msg = "Fail to openPort";
          break;
        case ErrorBeginCheckedBlock:
          msg = "Printer is offline (beginCheckedBlock)";
          break;
        case ErrorEndCheckedBlock:
          msg = "Printer is offline (endCheckedBlock)";
          break;
        case ErrorReadPort:
          msg = "Read port error (readPort)";
          break;
        case ErrorWritePort:
          msg = "Write port error (writePort)";
          break;
        default:
          msg = "Unknown error";
          break;
      }
      callbackContext.success(msg);
    } else {
      callbackContext.error("Expected one non-empty string argument.");
    }
  }

  private void coolMethod(String message, CallbackContext callbackContext) {
    if (message != null && message.length() > 0) {
      callbackContext.success(message);
    } else {
      callbackContext.error("Expected one non-empty string argument.");
    }
  }

  private void searchPos(CallbackContext callbackContext){
    ArrayList<PortInfo> ports = new ArrayList<PortInfo>();
    try {
      ports.addAll(StarIOPort.searchPrinter("BT:"));
    }
    catch (StarIOPortException e) {
    }
    try {
      ports.addAll(StarIOPort.searchPrinter("USB:"));
    }
    catch (StarIOPortException e) {
    }
    JSONArray portsArray = new JSONArray();
    for(PortInfo port:ports){
      JSONObject portObject = new JSONObject();
      try {
        portObject.put("name", port.getPortName());
        portObject.put("macAddress", port.getMacAddress());
        portsArray.put(portObject);
      }catch (JSONException e){
        callbackContext.error(e.toString());
      }
    }
    JSONObject returnObject = new JSONObject();

    try{
      returnObject.put("ports", portsArray);
    }catch(JSONException e){
      callbackContext.error(e.toString());
      return;
    }
    callbackContext.success(returnObject);

  }
  private void registerPrinter(String name, String macAddress,CallbackContext callbackContext){
    Activity activity = this.cordova.getActivity();
    PrinterSetting setting = new PrinterSetting(activity);
    setting.write(name, macAddress);

    if(PosManager.getPort()!= null){
      PosManager.disconnect();
    }

    PosManager = new StarIoExtManager(StarIoExtManager.Type.WithBarcodeReader,setting.getPortName(),setting.getPrinterType(),10000,this.cordova.getActivity());
    PosManager.setListener(mStarIoExtManagerListener);
    if(!setting.getPortName().isEmpty())
      PosManager.connect();

    JSONObject returnObject = new JSONObject();
    try{
      returnObject.put("name",name);
      returnObject.put("macAddress",macAddress);
    }catch(JSONException e){
      callbackContext.error(e.toString());
      return;
    }
    callbackContext.success(returnObject);
  }



  private void getPrinter(CallbackContext callbackContext){


    JSONObject returnObject = new JSONObject();
    try{
      returnObject.put("name", PosManager.getPort().getPortName());
    }catch(JSONException e){
      callbackContext.error(e.toString());
      return;
    }
    callbackContext.success(returnObject);
  }

  private void openCashDrawer(CallbackContext callbackContext){
    byte[]  data = PrinterFunctions.createCommandsOpenCashDrawer();
    Communication.Result result = Communication.sendCommandsDoNotCheckCondition(data, PosManager.getPort(), this.cordova.getActivity());
    callbackContext.success();
  }

  private void initManager(){
    Activity activity = this.cordova.getActivity();
    PrinterSetting setting = new PrinterSetting(activity);
    PosManager = new StarIoExtManager(StarIoExtManager.Type.WithBarcodeReader,setting.getPortName(),setting.getPrinterType(),10000,this.cordova.getActivity());
    PosManager.setListener(mStarIoExtManagerListener);
    if(!setting.getPortName().isEmpty())
      PosManager.connect();

  }

  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    initManager();

  }
}
