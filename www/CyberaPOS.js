var exec = require('cordova/exec');
var channel = require('cordova/channel');
//exports.onBarcodeDataReceive = function (success, error) {
channel.onCordovaReady.subscribe(function () {
    exec(function (result) {
        console.log("Barcode data recieved")
        console.log(result)
        cordova.fireWindowEvent('POS.barcodeRecieved', { 'data': +result });
    }, function (err) {
        console.log("Barcode data error")
        console.log(err)
        cordova.fireWindowEvent('POS.barcodeRecieved', { 'error': +err });
    }, "CyberaPOS", "onBarcodeDataReceive", [])

});
//}
    exports.coolMethod = function(arg0, success, error) {
        exec(success, error, "CyberaPOS", "coolMethod", [arg0]);
    };
    exports.searchPOS = function (success, error) {
        exec(success, error, "CyberaPOS", "searchPOS",[]);
    };
    exports.registerPOS = function (name, macAddress,modelName, success, error) {
        exec(success, error, "CyberaPOS", "registerPrinter", [name, macAddress,modelName]);
    };
    exports.getPOS = function (success, error) {
        exec(success, error, "CyberaPOS", "getPrinter", []);
    };
    exports.openCashDrawer = function (success, error) {
        exec(success, error, "CyberaPOS", "openCashDrawer", []);
    };
    exports.print = function (htmlToPrint, success, error) {
        exec(success, error, "CyberaPOS", "print", [htmlToPrint]);
    };
